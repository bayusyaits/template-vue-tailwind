import { defineStore } from "pinia";
import { Icon } from "../base-components/Lucide/Lucide.vue";

export interface Menu {
  icon: Icon;
  title: string;
  pageName?: string;
  subMenu?: Menu[];
  ignore?: boolean;
}

export interface TopMenuState {
  menu: Array<Menu>;
}

export const useTopMenuStore = defineStore("topMenu", {
  state: (): TopMenuState => ({
    menu: [
      {
        icon: "Inbox",
        pageName: "top-menu-components",
        title: "Components",
        subMenu: [
          {
            icon: "Activity",
            pageName: "top-menu-table",
            title: "Table",
            subMenu: [
              {
                icon: "Zap",
                pageName: "top-menu-regular-table",
                title: "Regular Table",
              },
              {
                icon: "Zap",
                pageName: "top-menu-tabulator",
                title: "Tabulator",
              },
            ],
          },
          {
            icon: "Activity",
            pageName: "top-menu-overlay",
            title: "Overlay",
            subMenu: [
              {
                icon: "Zap",
                pageName: "top-menu-modal",
                title: "Modal",
              },
              {
                icon: "Zap",
                pageName: "top-menu-slide-over",
                title: "Slide Over",
              },
              {
                icon: "Zap",
                pageName: "top-menu-notification",
                title: "Notification",
              },
            ],
          },
          {
            icon: "Activity",
            pageName: "top-menu-tab",
            title: "Tab",
          },
          {
            icon: "Activity",
            pageName: "top-menu-accordion",
            title: "Accordion",
          },
          {
            icon: "Activity",
            pageName: "top-menu-button",
            title: "Button",
          },
          {
            icon: "Activity",
            pageName: "top-menu-alert",
            title: "Alert",
          },
          {
            icon: "Activity",
            pageName: "top-menu-progress-bar",
            title: "Progress Bar",
          },
          {
            icon: "Activity",
            pageName: "top-menu-tooltip",
            title: "Tooltip",
          },
          {
            icon: "Activity",
            pageName: "top-menu-dropdown",
            title: "Dropdown",
          },
          {
            icon: "Activity",
            pageName: "top-menu-typography",
            title: "Typography",
          },
          {
            icon: "Activity",
            pageName: "top-menu-icon",
            title: "",
          },
          {
            icon: "Activity",
            pageName: "top-menu-loading-icon",
            title: "Loading ",
          },
        ],
      },
      {
        icon: "Sidebar",
        pageName: "top-menu-forms",
        title: "Forms",
        subMenu: [
          {
            icon: "Activity",
            pageName: "top-menu-regular-form",
            title: "Regular Form",
          },
          {
            icon: "Activity",
            pageName: "top-menu-datepicker",
            title: "Datepicker",
          },
          {
            icon: "Activity",
            pageName: "top-menu-tom-select",
            title: "Tom Select",
          },
          {
            icon: "Activity",
            pageName: "top-menu-file-upload",
            title: "File Upload",
          },
        ],
      },
      {
        icon: "HardDrive",
        pageName: "top-menu-widgets",
        title: "Widgets",
        subMenu: [
          {
            icon: "Activity",
            pageName: "top-menu-slider",
            title: "Slider",
          },
          {
            icon: "Activity",
            pageName: "top-menu-image-zoom",
            title: "Image Zoom",
          },
        ],
      },
    ],
  }),
});
