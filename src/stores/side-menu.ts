import { defineStore } from "pinia";
import { Icon } from "../base-components/Lucide/Lucide.vue";

export interface Menu {
  icon: Icon;
  title: string;
  pageName?: string;
  subMenu?: Menu[];
  ignore?: boolean;
}

export interface SideMenuState {
  menu: Array<Menu | "divider">;
}

export const useSideMenuStore = defineStore("sideMenu", {
  state: (): SideMenuState => ({
    menu: [
      {
        icon: "Inbox",
        pageName: "side-menu-components",
        title: "Components",
        subMenu: [
          {
            icon: "Activity",
            pageName: "side-menu-table",
            title: "Table",
            subMenu: [
              {
                icon: "Zap",
                pageName: "side-menu-regular-table",
                title: "Regular Table",
              },
              {
                icon: "Zap",
                pageName: "side-menu-tabulator",
                title: "Tabulator",
              },
            ],
          },
          {
            icon: "Activity",
            pageName: "side-menu-overlay",
            title: "Overlay",
            subMenu: [
              {
                icon: "Zap",
                pageName: "side-menu-modal",
                title: "Modal",
              },
              {
                icon: "Zap",
                pageName: "side-menu-slide-over",
                title: "Slide Over",
              },
              {
                icon: "Zap",
                pageName: "side-menu-notification",
                title: "Notification",
              },
            ],
          },
          {
            icon: "Zap",
            pageName: "side-menu-tab",
            title: "Tab",
          },
          {
            icon: "Zap",
            pageName: "side-menu-accordion",
            title: "Accordion",
          },
          {
            icon: "Zap",
            pageName: "side-menu-button",
            title: "Button",
          },
          {
            icon: "Zap",
            pageName: "side-menu-alert",
            title: "Alert",
          },
          {
            icon: "Zap",
            pageName: "side-menu-progress-bar",
            title: "Progress Bar",
          },
          {
            icon: "Zap",
            pageName: "side-menu-tooltip",
            title: "Tooltip",
          },
          {
            icon: "Zap",
            pageName: "side-menu-dropdown",
            title: "Dropdown",
          },
          {
            icon: "Zap",
            pageName: "side-menu-typography",
            title: "Typography",
          },
          {
            icon: "Zap",
            pageName: "side-menu-icon",
            title: "Icon",
          },
          {
            icon: "Zap",
            pageName: "side-menu-loading-icon",
            title: "Loading ",
          },
        ],
      },
      {
        icon: "Sidebar",
        pageName: "side-menu-forms",
        title: "Forms",
        subMenu: [
          {
            icon: "Activity",
            pageName: "side-menu-regular-form",
            title: "Regular Form",
          },
          {
            icon: "Activity",
            pageName: "side-menu-datepicker",
            title: "Datepicker",
          },
          {
            icon: "Activity",
            pageName: "side-menu-tom-select",
            title: "Tom Select",
          },
          {
            icon: "Activity",
            pageName: "side-menu-file-upload",
            title: "File Upload",
          },
        ],
      },
      {
        icon: "HardDrive",
        pageName: "side-menu-widgets",
        title: "Widgets",
        subMenu: [
          {
            icon: "Activity",
            pageName: "side-menu-slider",
            title: "Slider",
          },
          {
            icon: "Activity",
            pageName: "side-menu-image-zoom",
            title: "Image Zoom",
          },
        ],
      },
    ],
  }),
});
