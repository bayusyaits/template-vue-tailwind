import { defineStore } from "pinia";
import { Icon } from "../base-components/Lucide/Lucide.vue";

export interface Menu {
  icon: Icon;
  title: string;
  pageName?: string;
  subMenu?: Menu[];
  ignore?: boolean;
}

export interface SimpleMenuState {
  menu: Array<Menu | "divider">;
}

export const useSimpleMenuStore = defineStore("simpleMenu", {
  state: (): SimpleMenuState => ({
    menu: [
      {
        icon: "Inbox",
        pageName: "simple-menu-components",
        title: "Components",
        subMenu: [
          {
            icon: "Activity",
            pageName: "simple-menu-table",
            title: "Table",
            subMenu: [
              {
                icon: "Zap",
                pageName: "simple-menu-regular-table",
                title: "Regular Table",
              },
              {
                icon: "Zap",
                pageName: "simple-menu-tabulator",
                title: "Tabulator",
              },
            ],
          },
          {
            icon: "Activity",
            pageName: "simple-menu-overlay",
            title: "Overlay",
            subMenu: [
              {
                icon: "Zap",
                pageName: "simple-menu-modal",
                title: "Modal",
              },
              {
                icon: "Zap",
                pageName: "simple-menu-slide-over",
                title: "Slide Over",
              },
              {
                icon: "Zap",
                pageName: "simple-menu-notification",
                title: "Notification",
              },
            ],
          },
          {
            icon: "Activity",
            pageName: "simple-menu-tab",
            title: "Tab",
          },
          {
            icon: "Activity",
            pageName: "simple-menu-accordion",
            title: "Accordion",
          },
          {
            icon: "Activity",
            pageName: "simple-menu-button",
            title: "Button",
          },
          {
            icon: "Activity",
            pageName: "simple-menu-alert",
            title: "Alert",
          },
          {
            icon: "Activity",
            pageName: "simple-menu-progress-bar",
            title: "Progress Bar",
          },
          {
            icon: "Activity",
            pageName: "simple-menu-tooltip",
            title: "Tooltip",
          },
          {
            icon: "Activity",
            pageName: "simple-menu-dropdown",
            title: "Dropdown",
          },
          {
            icon: "Activity",
            pageName: "simple-menu-typography",
            title: "Typography",
          },
          {
            icon: "Activity",
            pageName: "simple-menu-icon",
            title: "",
          },
          {
            icon: "Activity",
            pageName: "simple-menu-loading-icon",
            title: "Loading ",
          },
        ],
      },
      {
        icon: "Sidebar",
        pageName: "simple-menu-forms",
        title: "Forms",
        subMenu: [
          {
            icon: "Activity",
            pageName: "simple-menu-regular-form",
            title: "Regular Form",
          },
          {
            icon: "Activity",
            pageName: "simple-menu-datepicker",
            title: "Datepicker",
          },
          {
            icon: "Activity",
            pageName: "simple-menu-tom-select",
            title: "Tom Select",
          },
          {
            icon: "Activity",
            pageName: "simple-menu-file-upload",
            title: "File Upload",
          },
        ],
      },
      {
        icon: "HardDrive",
        pageName: "simple-menu-widgets",
        title: "Widgets",
        subMenu: [
          {
            icon: "Activity",
            pageName: "simple-menu-slider",
            title: "Slider",
          },
          {
            icon: "Activity",
            pageName: "simple-menu-image-zoom",
            title: "Image Zoom",
          },
        ],
      },
    ],
  }),
});
