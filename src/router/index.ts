import { createRouter, createWebHistory } from "vue-router";
import SideMenu from "../layouts/SideMenu/SideMenu.vue";
import ErrorPage from "../pages/ErrorPage.vue";
import RegularTable from "../pages/RegularTable.vue";
import Modal from "../pages/Modal.vue";
import Slideover from "../pages/Slideover.vue";
import Notification from "../pages/Notification.vue";
import Tab from "../pages/Tab.vue";
import Accordion from "../pages/Accordion.vue";
import Button from "../pages/Button.vue";
import Alert from "../pages/Alert.vue";
import ProgressBar from "../pages/ProgressBar.vue";
import Tooltip from "../pages/Tooltip.vue";
import Dropdown from "../pages/Dropdown.vue";
import Icon from "../pages/Icon.vue";
import LoadingIcon from "../pages/LoadingIcon.vue";
import RegularForm from "../pages/RegularForm.vue";
import Datepicker from "../pages/Datepicker.vue";
import TomSelect from "../pages/TomSelect.vue";
import FileUpload from "../pages/FileUpload.vue";
import Slider from "../pages/Slider.vue";
import ImageZoom from "../pages/ImageZoom.vue";
import Tabulator from "../pages/Tabulator.vue";
import Typography from "../pages/Typography.vue";


const routes = [
  {
    path: "/",
    component: SideMenu,
    children: [
      {
        path: "regular-table",
        name: "side-menu-regular-table",
        component: RegularTable,
      },
      {
        path: "modal",
        name: "side-menu-modal",
        component: Modal,
      },
      {
        path: "slide-over",
        name: "side-menu-slide-over",
        component: Slideover,
      },
      {
        path: "notification",
        name: "side-menu-notification",
        component: Notification,
      },
      {
        path: "tab",
        name: "side-menu-tab",
        component: Tab,
      },
      {
        path: "accordion",
        name: "side-menu-accordion",
        component: Accordion,
      },
      {
        path: "button",
        name: "side-menu-button",
        component: Button,
      },
      {
        path: "alert",
        name: "side-menu-alert",
        component: Alert,
      },
      {
        path: "progress-bar",
        name: "side-menu-progress-bar",
        component: ProgressBar,
      },
      {
        path: "tooltip",
        name: "side-menu-tooltip",
        component: Tooltip,
      },
      {
        path: "dropdown",
        name: "side-menu-dropdown",
        component: Dropdown,
      },
      {
        path: "icon",
        name: "side-menu-icon",
        component: Icon,
      },
      {
        path: "loading-icon",
        name: "side-menu-loading-icon",
        component: LoadingIcon,
      },
      {
        path: "regular-form",
        name: "side-menu-regular-form",
        component: RegularForm,
      },
      {
        path: "datepicker",
        name: "side-menu-datepicker",
        component: Datepicker,
      },
      {
        path: "tom-select",
        name: "side-menu-tom-select",
        component: TomSelect,
      },
      {
        path: "file-upload",
        name: "side-menu-file-upload",
        component: FileUpload,
      },
      {
        path: "tabulator",
        name: "side-menu-tabulator",
        component: Tabulator,
      },
      {
        path: "typography",
        name: "side-menu-typography",
        component: Typography,
      },
      {
        path: "slider",
        name: "side-menu-slider",
        component: Slider,
      },
      {
        path: "image-zoom",
        name: "side-menu-image-zoom",
        component: ImageZoom,
      },
    ],
  },
  {
    path: "/error-page",
    name: "error-page",
    component: ErrorPage,
  },
  {
    path: "/:pathMatch(.*)*",
    component: ErrorPage,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
  scrollBehavior(to, from, savedPosition) {
    return savedPosition || { left: 0, top: 0 };
  },
});

export default router;
